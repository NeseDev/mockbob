module.exports = (text, tolerance = 2, startUppercase) => {
  tolerance = tolerance <= 10 ? tolerance : 10;

  let startCase = startUppercase != undefined ? startUppercase : Math.round(Math.random()) == true;
  let lastCases = new Array(tolerance).fill(startCase);

  const mocked = text.match(/./g).map(char => {
    if(lastCases.every(val => val == lastCases[0])) {
      let lastCase = lastCases.shift();
      lastCases.push(!lastCase);
      return !lastCase ? char.toLowerCase() : char.toUpperCase();
    } else {
      let curCase = Math.round(Math.random());
      lastCases.shift();
      lastCases.push(curCase == true);
      return curCase ? char.toLowerCase() : char.toUpperCase();
    }
  }).join('');
  
  return mocked;
}