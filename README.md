#MockBob
```js
const mockbob = require('mockbob');
let mockedText = mockbob(text[, tolerance:2, startUppercase]);
```
```js
// Mock text with default tolerance of 2 characters and random first Case
let mockedText = mockbob('I am autistic');
// => I aM AuTIsTiC
// => i Am AuTisTIc
// => I Am aUTiSTic
```
```js
// Mock text with a tolerance of 1 (every char will alter)
let mockedText = mockbob('I am autistic', 1);
// => i Am AuTiStIc
// => I aM aUtIsTiC
```
```js
// Mock text with a tolerance of 10 and start with a capital Letter
let mockedText = mockbob('I am autistic', 10, true);
// => I Am aUtISTIC
// => I am AUTIsTiC
// => I am AutiSTIc
```